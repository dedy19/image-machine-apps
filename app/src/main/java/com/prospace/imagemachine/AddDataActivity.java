package com.prospace.imagemachine;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;

public class AddDataActivity extends BaseActivity implements View.OnClickListener, Validator.ValidationListener{
    private static final String TAG = "AddDataActivity";
    private Calendar myCalendar;
    private DatabaseHelper mDatabaseHelper;


    @NotEmpty
    @Length(min = 3, max = 20)
    private EditText nameText, typeText;

    @NotEmpty
    private EditText codeNumber, lastMtnDate;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);
        initView();

        validator = new Validator(this);
        validator.setValidationListener(this);

        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.YEAR, year);

                lastMtnDate.setText(year + "/" + month + "/" + dayOfMonth);
            }
        };

        final DatePickerDialog dialog = new DatePickerDialog(AddDataActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        lastMtnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                lastMtnDate.setError(null);
            }
        });
        lastMtnDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                // showMyDialog();
                dialog.show();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
            }
        });
    }

    private void initView() {
        nameText = findViewById(R.id.editTextName);
        typeText = findViewById(R.id.editTextType);
        codeNumber = findViewById(R.id.editTextCode);
        lastMtnDate = findViewById(R.id.editTextLastMtn);
        Button submit = findViewById(R.id.submit_action);
        Button cancel = findViewById(R.id.cancel_action);
        mDatabaseHelper = new DatabaseHelper(this);

        submit.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    public void addData (String name, String type, String code, String last_mtn){
        boolean insertData = mDatabaseHelper.addData(name, type, code, last_mtn);
        Log.d(TAG, "Adding data process");
        if(insertData){
            Toast.makeText(this,"Data added successfully", Toast.LENGTH_SHORT).show();
            clearForm();
        }else{
            Toast.makeText(this,"Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit_action:
                submit_onClick(v);
                break;
            case R.id.cancel_action:
                clearForm();
                break;
            default:
        }

    }

    private void submit_onClick(View v) {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        String name = nameText.getText().toString();
        String type = typeText.getText().toString();
        String code = codeNumber.getText().toString();
        String lastMtn = lastMtnDate.getText().toString();
        addData(name, type, code, lastMtn);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors){
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            //Display error messages
            if(view instanceof EditText){
                ((EditText) view).setError(message);
            }else{
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void clearForm(){
        new android.os.Handler().postDelayed(
            new Runnable() {
                public void run() {
                    nameText.setText("");
                    typeText.setText("");
                    codeNumber.setText("");
                    lastMtnDate.setText("");
                }
            }, 2000);
    }


}
