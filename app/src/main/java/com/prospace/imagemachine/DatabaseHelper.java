package com.prospace.imagemachine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String DATABASE_NAME = "machine_database";
    private static final int DATABASE_VERSION = 2;

    private static final String TABLE_MACHINE = "machines";
    private static final String KEY_ID = "ID";
    private static final String KEY_NAME = "name";
    private static final String KEY_TYPE = "type";
    private static final String KEY_CODE = "qr_code";
    private static final String KEY_LAST_MAINT = "last_mtn";
    private static final String CREATE_TABLE_MACHINES = "CREATE TABLE "
                                                        + TABLE_MACHINE
                                                        + "("
                                                        + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                                        + KEY_NAME + " TEXT,"
                                                        + KEY_TYPE + " TEXT,"
                                                        + KEY_CODE + " INTEGER,"
                                                        + KEY_LAST_MAINT + " TEXT"
                                                        + ")";

    private static final String TABLE_MACHINE_IMAGES = "machines_images";
    private static final String KEY_MACHINE_ID = "machines_id";
    private static final String KEY_IMAGE = "image";
    private static final String CREATE_TABLE_MACHINE_IMAGES = "CREATE TABLE "
            + TABLE_MACHINE_IMAGES
            + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_MACHINE_ID + " INTEGER NOT NULL,"
            + KEY_IMAGE + " BLOB,"
            + "CONSTRAINT fk_machines"
            + " FOREIGN KEY (" + KEY_MACHINE_ID + ") "
            + " REFERENCES " + TABLE_MACHINE + " (" + KEY_ID + ")"
            + "ON DELETE CASCADE ON UPDATE RESTRICT"
            + ")";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MACHINES);
        db.execSQL(CREATE_TABLE_MACHINE_IMAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MACHINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MACHINE_IMAGES);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=on");
    }

    public boolean addData(String name, String type, String qr_code, String last_mtn){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.putNull(KEY_ID);
        contentValues.put(KEY_NAME, name);
        contentValues.put(KEY_TYPE, type);
        contentValues.put(KEY_CODE, Integer.parseInt(qr_code));
        contentValues.put(KEY_LAST_MAINT, getDateTime(last_mtn));

        Log.d(TAG, "addData: Adding data with name: " + name + " to " + TABLE_MACHINE);

        long result = db.insert(TABLE_MACHINE, null, contentValues);

        return result != -1;
    }

    public boolean updateData(int ID, String name, String type, int qr_code, String last_mtn){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        if(name != null)
            contentValues.put(KEY_NAME, name);
        if(type != null)
            contentValues.put(KEY_TYPE, type);
        if(qr_code != 0)
            contentValues.put(KEY_CODE, qr_code);
        if(last_mtn != null)
            contentValues.put(KEY_LAST_MAINT, getDateTime(last_mtn));

        Log.d(TAG, "updateData: Update data successful");

        long result = db.update(TABLE_MACHINE, contentValues, KEY_ID + " = ? ", new String[]{ String.valueOf(ID) });

        return result != -1;
    }

    public boolean insertImages(int ID, ArrayList<byte[]> imagesByte){
        long result = -1;
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        for(byte[] bytes : imagesByte){
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_MACHINE_ID, ID);
            contentValues.put(KEY_IMAGE, bytes);

            result = db.insert(TABLE_MACHINE_IMAGES, null, contentValues);
        }
        db.setTransactionSuccessful();
        db.endTransaction();

        return result != -1;
    }

    public ArrayList<Machine> getAllData(){
        ArrayList<Machine> machineArrayList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_MACHINE;
        Cursor cursor = db.rawQuery(query , null);

        //looping through all rows and adding to the list
        if(cursor.moveToFirst()){
            do{
                Machine machine = new Machine();
                machine.setID(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                machine.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                machine.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));

                machineArrayList.add(machine);
            }while (
                cursor.moveToNext()
            );
        }
        cursor.close();
        return machineArrayList;
    }

    public Machine getDataByKey(String key, int value){
        Machine machine = new Machine();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " +  TABLE_MACHINE + " WHERE " + key + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{ String.valueOf(value)});
        if(cursor.moveToFirst()){
            int ID = cursor.getInt(cursor.getColumnIndex(KEY_ID));
            do{
                machine.setID(ID);
                machine.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                machine.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                machine.setCode(cursor.getInt(cursor.getColumnIndex(KEY_CODE)));
                machine.setLastMaint(cursor.getString(cursor.getColumnIndex(KEY_LAST_MAINT)));

                ArrayList<ImageModel> bitmapArrayList = new ArrayList<>();
                String imagesQuery = "SELECT * FROM " + TABLE_MACHINE_IMAGES + " WHERE " + KEY_MACHINE_ID + " = ?";
                Cursor cursorImages = db.rawQuery(imagesQuery, new String[]{ String.valueOf(ID)});
                if(cursorImages.moveToFirst()){
                    do {
                            ImageModel imageModel = new ImageModel();
                            imageModel.setId(cursorImages.getInt(cursorImages.getColumnIndex(KEY_ID)));
                            byte[] imageByte = cursorImages.getBlob(cursorImages.getColumnIndex(KEY_IMAGE));
                            Bitmap imageBitmap = Utility.byteToBitmap(imageByte);
                            imageModel.setBitmap(imageBitmap);

                            bitmapArrayList.add(imageModel);
                    }while (
                        cursorImages.moveToNext()
                    );
                }
                cursorImages.close();

                machine.setImages(bitmapArrayList);
            }while (
                cursor.moveToNext()
            );
        }
        cursor.close();
        return machine;
    }

    public boolean deleteAllData(String TABLE_NAME ){
        SQLiteDatabase db = getWritableDatabase();
        long result = db.delete(TABLE_NAME, "1", null);
        return result != -1;
    }

    public boolean deleteDataById(int ID){
        SQLiteDatabase db = getWritableDatabase();
        long result = db.delete(TABLE_MACHINE,KEY_ID + " = ? ", new String[]{String.valueOf(ID)});
        return result != -1;
    }

    public boolean deleteImagesByIds(String[] ID){

        String whereClause = KEY_ID + " IN (" + new String(new char[ID.length-1]).replace("\0", "?,") + "?)";
        SQLiteDatabase db = getWritableDatabase();
        long result = db.delete(TABLE_MACHINE_IMAGES, whereClause, ID);

        return result != -1;
    }

    private String getDateTime(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date(dateString);
        return dateFormat.format(date);
    }
}
