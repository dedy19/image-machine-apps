package com.prospace.imagemachine;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class EditActivity extends BaseActivity implements View.OnClickListener {

    Calendar myCalendar;
    DatabaseHelper mDatabaseHelper;
    EditText editName, editType, editCode, editMaint;
    Button cancelBtn, updateBtn;
    String name, type, lastMaint;
    int year, month, dayMonth, ID, code;
    Intent returnIntent;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle bundle = getIntent().getBundleExtra("bundle");
        ID = bundle.getInt("ID");
        name = bundle.getString("name");
        type = bundle.getString("type");
        code = bundle.getInt("code");
        lastMaint = bundle.getString("date");

        mDatabaseHelper = new DatabaseHelper(this);

        editName = findViewById(R.id.editTextName);
        editType = findViewById(R.id.editTextType);
        editCode= findViewById(R.id.editTextCode);
        editMaint= findViewById(R.id.editTextLastMtn);
        
        editName.setText(name);
        editCode.setText(String.valueOf(code));
        editType.setText(type);
        lastMaint = lastMaint.replace("-","/");
        editMaint.setText(lastMaint);

        myCalendar = Calendar.getInstance();
        returnIntent = new Intent();

        String stringMaint = editMaint.getText().toString();
        String[] currentDate = stringMaint.split("/");
        year = Integer.parseInt(currentDate[0]);
        month = Integer.parseInt(currentDate[1]) - 1;
        dayMonth = Integer.parseInt(currentDate[2]);


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.YEAR, year);

                editMaint.setText(year + "/" + month + "/" + dayOfMonth );
            }
        };

        final DatePickerDialog dialog = new DatePickerDialog(EditActivity.this, date, year, month, dayMonth);

        dialog.onDateChanged(new DatePicker(this), year, month, dayMonth);
        editMaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();

            }
        });
        editMaint.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                    dialog.show();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }
            }
        });

        cancelBtn = findViewById(R.id.cancel_action);
        updateBtn = findViewById(R.id.update_action);
        
        cancelBtn.setOnClickListener(this);
        updateBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancel_action:
                setResult(RESULT_CANCELED, returnIntent);
                finish();
                break;
            case R.id.update_action:
                name = editName.getText().toString();
                type = editType.getText().toString();
                code = Integer.parseInt(editCode.getText().toString());
                lastMaint = editMaint.getText().toString();
                updateData(name, type, code, lastMaint);
                break;
            default:
        }
    }

    private void updateData(String name, String type, int code, String lastMaint) {
        boolean updateData = mDatabaseHelper.updateData(ID, name,type,code,lastMaint);
        if(updateData){
            Toast.makeText(this,"Data updated successfully", Toast.LENGTH_LONG).show();
            setResult(RESULT_OK, returnIntent);
            finish();
        }else{
            Toast.makeText(this,"Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
        }
    }
}
