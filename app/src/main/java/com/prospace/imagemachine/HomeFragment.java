package com.prospace.imagemachine;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    TableLayout machine_table;
    String sortName;
    DatabaseHelper mDatabaseHelper;
    ArrayList<Machine> arrayList;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        arrayList = mDatabaseHelper.getAllData();
        fillMachineTable(arrayList, sortName);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sortName = "name"; //default sort table;
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        machine_table = view.findViewById(R.id.machine_table);
        Button addButton = view.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddDataActivity.class);
                startActivity(intent);
            }
        });

        mDatabaseHelper = new DatabaseHelper(this.getContext());

        arrayList = mDatabaseHelper.getAllData();
        fillMachineTable(arrayList, sortName);
        final ToggleButton sortButton = view.findViewById(R.id.sort_button);

        sortButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    sortName = "type";
                }else{
                    sortName = "name";
                }
                fillMachineTable(arrayList, sortName);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("machine.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    void fillMachineTable(ArrayList<Machine> arrayList, String sortName){
        TableRow row;
        TextView name, type;
        //converting to dip unit
        int dip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 1, getResources().getDisplayMetrics());
        machine_table.removeAllViews();
        if(arrayList.size() > 0 ){
            Collections.sort(arrayList, new SortBy(sortName));
            for (int i = 0; i < arrayList.size(); i++) {
                final Machine data = arrayList.get(i);
                String machine_name = data.getName();
                String machine_type = data.getType();

                View line = new View(this.getContext());
                line.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dip));
                line.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

                row = new TableRow(this.getContext());

                name = new TextView(this.getContext());
                type = new TextView(this.getContext());
                type.setBackgroundColor(getResources().getColor(R.color.white));

                name.setText(machine_name);
                type.setText(machine_type);

                name.setGravity(Gravity.CENTER_VERTICAL);
                type.setGravity(Gravity.CENTER_VERTICAL);

                name.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 50 * dip));
                type.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 50 * dip));

                name.setClickable(true);
                name.setBackground(getResources().getDrawable(R.drawable.row_selector));
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        machineDetailPage(v, data);
                    }
                });


                name.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                type.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                row.addView(name);
                row.addView(type);

                machine_table.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                machine_table.addView(line);
            }
        }else{
            row = new TableRow(this.getContext());

            name = new TextView(this.getContext());
            name.setText("No Data Available");
            name.setGravity(Gravity.CENTER_VERTICAL);
            name.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 50 * dip));
            name.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            row.addView(name);

            machine_table.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

            View line = new View(this.getContext());
            line.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dip));
            line.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            machine_table.addView(line);
        }
    }

    public void machineDetailPage(View view, Machine data){
        Intent intent = new Intent(getActivity(),MachineDetail.class);
        int ID = data.getId();
        intent.putExtra("ID", ID);
        startActivity(intent);
    }
}
