package com.prospace.imagemachine;

import android.graphics.Bitmap;

import java.io.Serializable;

public class ImageModel implements Serializable {
    int id;
    Bitmap bitmap;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
