package com.prospace.imagemachine;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;

public class Machine implements Serializable {
    private int ID, code;
    private String name, type, lastMaint;
    private ArrayList<ImageModel> images;

    public int getId(){
        return ID;
    }
    public void setID(int ID){
        this.ID = ID;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getType(){
        return type;
    }
    public void setType(String type){
        this.type = type;
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }

    public String getLastMaint() {
        return lastMaint;
    }
    public void setLastMaint(String lastMaint){
        this.lastMaint = lastMaint;
    }

    public ArrayList<ImageModel> getImages() {

        return images;
    }
    public void setImages(ArrayList<ImageModel> images)
    {
        this.images = images;
    }
}
