package com.prospace.imagemachine;

import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MachineDetail extends BaseActivity implements View.OnClickListener {

    private static final int PICK_IMAGE_REQUEST = 1;  // The request code
    private static final int EDIT_DATA_REQUEST = 2;
    private static final String KEY_ID = "ID";
    private Calendar myCalendar;
    private AlphaAnimation buttonClick;
    private LinearLayout thumbnail;
    private LinearLayout.LayoutParams layoutParams, checkboxParams, params;
    private int dip, widthScreen, marginThumbnail, widthThumbnail, heightThumbnail, divideScreen, checkboxMargin, dataID, stringId, stringCode ;
    private Display display;
    private Point size;
    private boolean isOnLongClick = false;
    private Button deleteImages, deleteData, editData;
    private String stringName, stringType, stringLastMaint;
    private DatabaseHelper mDatabaseHelper;
    private int[] idx;
    private String[] splitImages;
    private ImageView noImage;
    private ArrayList<ImageModel> images;
    private Machine data;
    private TextView tvId, name, type, code, last_maintenance;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_detail);

        mDatabaseHelper = new DatabaseHelper(this);
        sharedPreferences = getSharedPreferences(KEY_ID, Context.MODE_PRIVATE);

        myCalendar = Calendar.getInstance();

        buttonClick = new AlphaAnimation(1F, 0.7F);

         tvId = findViewById(R.id.machine_id);
         name = findViewById(R.id.machine_name);
         type = findViewById(R.id.machine_type);
         code = findViewById(R.id.machine_code);
        noImage = findViewById(R.id.no_image);
         last_maintenance = findViewById(R.id.last_maintenance);

        FloatingActionButton galleryButton = findViewById(R.id.image_button);
        galleryButton.setOnClickListener(this);

        deleteData = findViewById(R.id.delete_button);
        deleteData.setOnClickListener(this);

        deleteImages = findViewById(R.id.delete_images_btn);
        deleteImages.setOnClickListener(this);

        editData = findViewById(R.id.edit_button);
        editData.setOnClickListener(this);

        thumbnail = findViewById(R.id.thumbnail);

        dip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 1, getResources().getDisplayMetrics()); //convert unit to sp

        //get screen size
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        widthScreen = size.x;

        //set dimension of thumbnail
        divideScreen = 2;
        marginThumbnail = widthScreen/divideScreen * 5/100;
        widthThumbnail = widthScreen/divideScreen - (marginThumbnail * 2);
        heightThumbnail = widthScreen/divideScreen - (marginThumbnail * 2);

        layoutParams = new LinearLayout.LayoutParams(widthThumbnail,heightThumbnail);
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.setMargins(marginThumbnail, marginThumbnail, marginThumbnail, marginThumbnail);

        checkboxMargin = 25 * dip;
        checkboxParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        checkboxParams.setMargins(checkboxMargin, checkboxMargin,checkboxMargin,checkboxMargin);

        dataID = getIntent().getIntExtra("ID",0);

        if(dataID == 0){
            dataID = sharedPreferences.getInt(KEY_ID, 0);
        }

        data = mDatabaseHelper.getDataByKey("ID", dataID);

        stringId = data.getId();
        stringName = data.getName();
        stringType = data.getType();
        stringCode = data.getCode();
        stringLastMaint = data.getLastMaint();

        tvId.setText(String.valueOf(stringId));
        name.setText(stringName);
        type.setText(stringType);
        code.setText(String.valueOf(stringCode));

        stringLastMaint = stringLastMaint.replace("-","/");
        last_maintenance.setText(stringLastMaint);

        images = data.getImages();

        if(images.size() > 0){
            noImage.setVisibility(View.GONE);
            idx = new int[images.size()];
            int i = 0;
            for(ImageModel imageModel : images){
                setImagesView(idx, i, imageModel);
                i++;
            }
        }else{
            noImage.setVisibility(View.VISIBLE);
        }

        final String[] time = stringLastMaint.split("/");
        int year = Integer.parseInt(time[0]);
        int month = Integer.parseInt(time[1]) - 1;
        int dayMonth = Integer.parseInt(time[2]);

        last_maintenance.setClickable(true);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            }
        };
        final DatePickerDialog dialog = new DatePickerDialog(
                MachineDetail.this, date, year, month, dayMonth
        );

        Date dateCurrent = new Date(year,month,dayMonth);
        long timeInMilis = dateCurrent.getTime();

        dialog.getDatePicker().setMinDate(timeInMilis);
        dialog.getDatePicker().setMaxDate(timeInMilis);

        last_maintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                dialog.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( resultCode == RESULT_OK){
            if (requestCode == PICK_IMAGE_REQUEST) {

                if(data != null){
                    ArrayList<byte[]> imagesArray = new ArrayList<>();
                    ClipData clipData = data.getClipData();
                    if(clipData != null){
                        if(clipData.getItemCount() <= 10){
                            noImage.setVisibility(View.GONE);
                            idx = new int[clipData.getItemCount()];

                            for(int i=0; i < clipData.getItemCount(); i++){
                                ClipData.Item item = clipData.getItemAt(i);
                                Uri uri = item.getUri();
                                try {
                                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                                    byte[] imagesByte = Utility.bitmapToByte(bitmap);
                                    imagesArray.add(imagesByte);
                                }catch (IOException e){
                                    e.printStackTrace();
                                }
                            }
                            updateImages(stringId, imagesArray);
                        }else{
                            Toast.makeText(this,"Maximum 10 picture allowed. Please try again!",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Uri uri = data.getData();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            byte[] imagesByte = Utility.bitmapToByte(bitmap);
                            imagesArray.add(imagesByte);
                            updateImages(stringId, imagesArray);
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }
            }else if(requestCode == EDIT_DATA_REQUEST){
                finish();
                startActivity(getIntent());
            }
        }
    }

    private void setImagesView (final int[] id, int i, ImageModel imageModel){
        final Bitmap bitmap = imageModel.getBitmap();
        int imageId = imageModel.getId();
        RelativeLayout relativeLayout = new RelativeLayout(this);
        final ImageView newImage = new ImageView(this);
        final CheckBox checkBox = new CheckBox(this);
        checkBox.setLayoutParams(checkboxParams);
        id[i] = imageId;
        checkBox.setId(id[i]);
        checkBox.setButtonDrawable(getResources().getDrawable(R.drawable.custom_checkbox));
        checkBox.setVisibility(View.GONE);
        newImage.setLayoutParams(layoutParams);
        newImage.setContentDescription("Thumbnail");
        newImage.setImageBitmap(bitmap);
        //newImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_background));
        newImage.setClickable(true);
        newImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                if(!isOnLongClick)
                    fullScreenImage(bitmap);
                else
                    checkBox.setChecked(!checkBox.isChecked());

            }

        });

        newImage.setLongClickable(true);
        newImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                v.startAnimation(buttonClick);
                isOnLongClick = !isOnLongClick;
                int visibility;

                if(isOnLongClick){
                    visibility = View.VISIBLE;
                    checkBox.setChecked(!checkBox.isChecked());
                }else{
                    visibility = View.GONE;
                    for(int j=0; j < id.length; j++){
                        CheckBox showCheckBox = findViewById(id[j]);
                        showCheckBox.setChecked(false);
                    }
                }
                checkBox.setVisibility(visibility);
                for(int j=0; j < id.length; j++){
                    CheckBox showCheckBox = findViewById(id[j]);
                    showCheckBox.setVisibility(visibility);
                }

                deleteImages.setVisibility(visibility);

                return true;
            }
        });

        relativeLayout.addView(newImage);
        relativeLayout.addView(checkBox);
        thumbnail.addView(relativeLayout);
    }

    private void pickImage() {
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.putExtra(Intent.CATEGORY_OPENABLE, true);
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void fullScreenImage(Bitmap bitmap){
        Intent intent = new Intent(MachineDetail.this, FullscreenActivity.class);
        intent.putExtra("imageSrc", Utility.bitmapToByte(bitmap));
        startActivity(intent);
    }

    private void updateImages(int ID, ArrayList<byte[]> imageByte){
        boolean updateData = mDatabaseHelper.insertImages(ID, imageByte);

        if(updateData){
            Toast.makeText(this,"Data updated successfully", Toast.LENGTH_LONG).show();
            finish();
            startActivity(getIntent());
        }else{
            Toast.makeText(this,"Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        }
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onClick(View v) {
        v.startAnimation(buttonClick);
        switch (v.getId()){
            case R.id.image_button:
                pickImage();
                break;
            case R.id.delete_images_btn:
                deleteSelectedImage();
                break;
            case R.id.delete_button:
                deleteCurrentData(stringId);
                break;
            case R.id.edit_button:
                editCurrentData();
                break;
            default:

        }
    }

    private void editCurrentData() {
        Intent intent = new Intent(MachineDetail.this, EditActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ID, data.getId());
        bundle.putString("name", data.getName());
        bundle.putString("type", data.getType());
        bundle.putInt("code", data.getCode());
        bundle.putString("date", data.getLastMaint());
        intent.putExtra("bundle", bundle);
        startActivityForResult(intent, EDIT_DATA_REQUEST);
    }

    private void deleteCurrentData(int ID) {
        boolean deleteData = mDatabaseHelper.deleteDataById(ID);
        if(deleteData){
            Toast.makeText(this,"Data deleted successfully", Toast.LENGTH_LONG).show();
            finish();
        }else{
            Toast.makeText(this,"Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    private void deleteSelectedImage() {
        String[] idxToDelete = new String[idx.length];
        for(int i=0; i < idx.length; i++){
            CheckBox checkBox = findViewById(idx[i]);
            if(checkBox.isChecked()){
                idxToDelete[i] = String.valueOf(idx[i]);
            }
        }

        boolean deleteImages = mDatabaseHelper.deleteImagesByIds(idxToDelete);

        if(deleteImages){
            finish();
            startActivity(getIntent());
            overridePendingTransition(0,0);
            Toast.makeText(this,"Images deleted successfully", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this,"Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, dataID);
        editor.apply();
    }

}
