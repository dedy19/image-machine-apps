package com.prospace.imagemachine;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

public class SortBy implements Comparator<Machine>{
    String sortBy;

    public SortBy(String sortBy) {
        super();
        this.sortBy = sortBy;
    }

    @Override
    public int compare(Machine o1, Machine o2) {
        String first, second;

        if(sortBy == "name"){
            first = o1.getName();
            second = o2.getName();
        }else{
            first = o1.getType();
            second = o2.getType();
        }

        int result = first.compareTo(second);
        return result;
    }
}
