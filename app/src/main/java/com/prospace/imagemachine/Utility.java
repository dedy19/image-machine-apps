package com.prospace.imagemachine;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class Utility {
    public static Bitmap byteToBitmap(byte[] imageByte){
        return BitmapFactory.decodeByteArray(imageByte, 0 ,imageByte.length);
    }

    public static byte[] bitmapToByte(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

        return byteArrayOutputStream.toByteArray();
    }
}
